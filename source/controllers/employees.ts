/** source/controllers/employees.ts */
import e, { Request, Response, NextFunction } from 'express';
import axios, { AxiosResponse } from 'axios';
import fs from "fs";

async function getEmployees(req: Request, res: Response) {
    try {
      suceessMessage({ res, responseData: getEmployeeArr()});
    }catch (err) {
        errorMessage(res, err);
    }
}

/** 
 * @param res 
 * @param req
 * This function will return list of individual emp details.
 */ 
const getEmployee = async (req: Request, res: Response) => {
    try {
        suceessMessage({ res, responseData: filterEmployee(getEmployeeArr(), req.params.employeeId).length == 0 ? null : filterEmployee(getEmployeeArr(), req.params.employeeId)});
    }catch (err) {
        errorMessage(res, err);
    }
};

/**
 * @returns List of all employes array
 */
function getEmployeeArr(){
    const employeeJson = fs.readFileSync('source/employees-data.json',
        { encoding: 'utf8', flag: 'r' });
    var employeeArray = JSON.parse(employeeJson);
    return employeeArray;
}

/**
 * @param res String
 * @param responseData String
 * @returns This function returns success response with respective success.
 * either it will return array object or message.
*/
function suceessMessage({ res, responseData }){
    return res.status(200).json({
        data: responseData == null ? "No record found." : responseData
     });
}

/** 
 * @param res 
 * @param err 
 * @returns This function return error message with respective to 500 error code
 */
function errorMessage(res: Response<any, Record<string, any>>, err: any) {
    return res.status(500).json({ error: `Please try again. ${err}` });
};

/**
 * @param empArray Array
 * @param empId Number
 * @return 
 * This function accepts above mentioned params and return particular employee detail.
**/
function filterEmployee(empArray: Array<any>, empId: string): any {
    var filterEmpArray = empArray.filter(function (element) {
        return element.employeeId == empId ? element.employeeId : '';
    });
    return filterEmpArray.length != 0 ? filterEmpArray : [];    
}

function searchFilter(searchObject: { employeeId: any; firstName: any; lastName: any; department: any; status: any; }, sortByArr) {
    var filterEmpArray = [];
    filterEmpArray = getEmployeeArr().filter(function (element) {
        if(searchObject.employeeId  == element.employeeId && searchObject.employeeId !=undefined &&  searchObject.employeeId !=null){
           var empId = searchObject.employeeId;
        }else{
            var empId = null;
        }
        if(searchObject.firstName  == element.firstName && searchObject.firstName!=undefined && searchObject.firstName !=null){
            var firstName = searchObject.employeeId;
        }else{
            var firstName = null;
        }
        if(searchObject.lastName  == element.lastName && searchObject.lastName !=undefined && searchObject.lastName !=null){
            var lname = searchObject.lastName;
        }else{
            var lname = null;
        }
        if(searchObject.department  == element.department && searchObject.department !=undefined && searchObject.department !=null){
            var department = searchObject.department;
        }else{
            var department = null;
        }
        if(searchObject.status  == element.status && searchObject.status !=undefined && searchObject.status !=null){
            var status = searchObject.status;
        }else{
            var status = null;
        }
        return  empId !=null|| firstName !=null  || lname !=null || department !=null || status !=null ? element.employeeId : '';
    });
    return  filterEmpArray.length == 0 ? sortBy(getEmployeeArr(), sortByArr) : sortBy(filterEmpArray, sortByArr);
}


const searchEmployee = async (req: Request, res: Response) => {
    try {
       suceessMessage({ res, responseData: searchFilter(req.body.search, req.body.sortBy)});
    } catch (err) {
        errorMessage(res, err);
    }
};

function sortBy (arr, keys, splitKeyChar='~') {
    return arr.sort((i1,i2) => {
        const sortStr1 = keys.reduce((str, key) => str + splitKeyChar+i1[key], '')
        const sortStr2 = keys.reduce((str, key) => str + splitKeyChar+i2[key], '')
        return sortStr1.localeCompare(sortStr2)
    })
}

export default { getEmployees, getEmployee, searchEmployee };