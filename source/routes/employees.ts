/** source/routes/employees.ts */
import express from 'express';
import controller from '../controllers/employees';
const router = express.Router();

router.get('/api/v1/employees', controller.getEmployees);
router.get('/api/v1/employees/:employeeId', controller.getEmployee);
router.post('/api/v1/employees/search', controller.searchEmployee);

export = router;